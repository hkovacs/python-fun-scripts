
'''
convert a multi word string into an acronmy
'''

# Ask for a string
orig_string = input("Provide a string to convert: ")

# make the string uppercase
orig_string = orig_string.upper()

# split into a list
list_of_words = orig_string.split()

# loop through the list
for word in list_of_words:
    # get first letter of each word
    print(word[0], end="")
