
'''
compound interest calculator
'''

# ask for money invested and interest rate
money = float(input("how much to invest? "))

interest_rate = input("at what interest rate? ")
interest_rate = float(interest_rate) * .01

# calculate the result
for i in range(10):
    money = money + (money * interest_rate)

# output the result
print("investment after 10 years: {:.2f}".format(money))




