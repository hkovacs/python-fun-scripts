
'''
print a tree
'''

# get number of rows for tree
tree_height = int(input("How tall is the tree? "))

# get starting spaces for top of tree
spaces = tree_height - 1

# set hashes count
hashes = 1

# set stump spaces
stump_spaces = tree_height - 1

# print correct number of rows
while tree_height != 0:

# print the spaces
    for i in range(spaces):
        print(" ", end='')

# print the hashes
    for i in range(hashes):
        print("#", end='')

# print new line
    print("")

# decrement spaces
    spaces -= 1

# increment hashes
    hashes += 2

# decrement tree_height to exit loop
    tree_height -= 1

# print the spaces and hash for the stump
for i in range(stump_spaces):
    print(" ", end="")

print("#")




